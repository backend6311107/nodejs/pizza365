const express = require("express");
const mongoose = require("mongoose");
const path = require("path");
const app = express();
const port = 8000;
mongoose.set('strictQuery', true); //Để khi run không bị cảnh báo mongodb

const cors = require('cors');
app.use(cors());

app.use(express.static("app"));
app.use(express.json());

const drinkRouter = require("./app/routes/drinkRouter");
const voucherRouter = require("./app/routes/voucherRouter");
const userRouter = require("./app/routes/userRouter");
const orderRouter = require("./app/routes/orderRouter");

app.get("/", (req, res) => {
    console.log(`Link: ${__dirname}`);
    res.sendFile(path.join(__dirname, "../Pizza365/app/views/Task 43.80.html"))
})

app.use((req, res, next) => {
    console.log(`Current time: ${new Date()}`);
    next();
})

app.use((req, res, next) => {
    console.log(`Request method: ${req.method}`);
    next();
})

app.use("", drinkRouter);
app.use("", voucherRouter);
app.use("", userRouter);
app.use("", orderRouter);

mongoose.connect("mongodb://127.0.0.1:27017/Ex_Pizza365", (error) => {
    if (error) throw error;
    console.log(`Connect MongoDB successfully !!!`);
})

app.listen(port, () => {
    console.log(`App running in port: ${port}`);
})