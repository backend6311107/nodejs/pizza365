const express = require("express");
const router = express.Router();
const drinkMiddleware = require("../middlewares/drinkMiddleware");
const drinkController = require("../controllers/drinkController");

router.get("/drinks", drinkMiddleware.getAllDrinkMiddleware, drinkController.getAllDrink);

router.get("/drinks/:drinkId", drinkMiddleware.getDrinkIdMiddleware, drinkController.getDrinkById);

router.post("/drinks", drinkMiddleware.createDrinkMiddleware, drinkController.createDrink);

router.put("/drinks/:drinkId", drinkMiddleware.updateDrinkMiddleware, drinkController.updateDrinkById);

router.delete("/drinks/:drinkId", drinkMiddleware.deleteDrinkMiddleware, drinkController.deleteDrinkById);

module.exports = router;