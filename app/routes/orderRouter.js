const express = require("express");
const router = express.Router();
const orderMiddleware = require("../middlewares/orderMiddleware");
const orderController = require("../controllers/orderController");

router.get("/orders", orderMiddleware.getAllOrderMiddleware, orderController.getAllOrder);

router.get("/orders/:orderId", orderMiddleware.getOrderByIdMiddleware, orderController.getOrderById);

router.get("/users/:userId/orders", orderMiddleware.getAllOrderOfUserMiddleware, orderController.getAllOrderOfUser);

router.post("/users/:userId/orders", orderMiddleware.createOrderOfUserMiddleware, orderController.createOrderOfUser);

router.put("/orders/:orderId", orderMiddleware.updateOrderByIdMiddleware, orderController.updateOrderById);

router.delete("/users/:userId/orders/:orderId", orderMiddleware.deleteOrderByIDMiddleware, orderController.deleteOrderByID);

module.exports = router;