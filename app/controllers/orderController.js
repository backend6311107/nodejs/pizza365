const mongoose = require("mongoose");
const orderModel = require("../models/orderModel");
const userModel = require("../models/userModel");
const voucherModel = require("../models/voucherModel");
const drinkModel = require("../models/drinkModel");


const getAllOrder = (req, res) => {
    orderModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: "Get all order successfully.",
            data: data
        })
    })
}

const getOrderById = (req, res) => {
    const orderID = req.params.orderId;

    if (!mongoose.Types.ObjectId.isValid(orderID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `OrderID: ${orderID} không hợp lệ`
        })
    }

    orderModel.findById(orderID, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: `Get order by ID: ${orderID} successfully.`,
            data: data
        })
    })
}

const getAllOrderOfUser = (req, res) => {
    const userID = req.params.userId;

    if (!mongoose.Types.ObjectId.isValid(userID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `User ID: ${userID} không hợp lệ`
        })
    }

    userModel.findById(userID).populate("orders").exec((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: `Get all order of user ID: ${userID} successfully`,
            data: data
        })
    })
}

const createOrderOfUser = async (req, res) => {
    const userID = req.params.userId;
    const body = req.body;

    if (!mongoose.Types.ObjectId.isValid(userID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `UserID: ${userID} không hợp lệ`
        })
    }
    if (!body.pizzaSize || body.pizzaSize.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "Pizza Size Không hợp lệ"
        })
    }
    if (!body.pizzaType || body.pizzaType.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "Pizza Type không hợp lệ"
        })
    }
    if (!body.status || body.status.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "Status không hợp lệ"
        })
    }

    if (body.voucher) {
        const voucherID = await voucherModel.findOne({ _id: mongoose.Types.ObjectId(body.voucher) });
        if (!voucherID) {
            return res.status(400).json({
                status: "Bad request",
                message: "VoucherID không hợp lệ"
            });
        }
    }

    if (body.drink) {
        const drinkID = await drinkModel.findOne({ _id: mongoose.Types.ObjectId(body.drink) })
        if (!drinkID) {
            return res.status(400).json({
                status: "Bad request",
                message: "DrinkID không hợp lệ"
            });
        }
    }

    const newOrder = {
        _id: mongoose.Types.ObjectId(),
        pizzaSize: body.pizzaSize,
        pizzaType: body.pizzaType,
        voucher: body.voucher ? mongoose.Types.ObjectId(body.voucher) : null,
        drink: body.drink ? mongoose.Types.ObjectId(body.drink) : null,
        status: body.status
    }
    orderModel.create(newOrder, (err, data) => {
        if (err) {
            return res.status(400).json({
                status: "Internal server error",
                message: err.message
            })
        }
        userModel.findByIdAndUpdate(userID, { $push: { orders: data._id } }, (error) => {
            if (error) {
                return res.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return res.status(200).json({
                statuts: "Create new order successfully",
                data: data
            })
        })
    })
}

const updateOrderById = (req, res) => {
    const orderID = req.params.orderId;
    const body = req.body;

    if (!mongoose.Types.ObjectId.isValid(orderID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `OrderID: ${orderID} không hợp lệ`
        })
    }
    if (body.pizzaSize !== undefined && body.pizzaSize.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "pizzaSize không hợp lệ"
        })
    }
    if (body.pizzaType !== undefined && body.pizzaType.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "pizzaType Không hợp lệ"
        })
    }
    if (body.status !== undefined && body.status.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "status Không hợp lệ"
        })
    }

    orderUpdate = {};
    if (body.pizzaSize !== undefined) {
        orderUpdate.pizzaSize = body.pizzaSize;
    }
    if (body.pizzaType !== undefined) {
        orderUpdate.pizzaType = body.pizzaType;
    }
    if (body.status !== undefined) {
        orderUpdate.status = body.status;
    }
    orderModel.findByIdAndUpdate(orderID, orderUpdate, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: `Update order by ID: ${orderID} successfully.`,
            data: data
        })
    })
}

const deleteOrderByID = (req, res) => {
    const orderID = req.params.orderId;
    const userID = req.params.userId;

    if (!mongoose.Types.ObjectId.isValid(orderID)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `order ID: ${orderID} không hợp lệ`
        })
    }
    if (!mongoose.Types.ObjectId.isValid(userID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `User ID: ${userID} Không hợp lệ`
        })
    }

    orderModel.findByIdAndDelete(orderID, (error) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        userModel.findByIdAndUpdate(userID, { $pull: { orders: orderID } }, (err) => {
            if (err) {
                return res.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return res.status(200).json({
                status: `Delete order ID: ${orderID} successfully`
            })
        })
    })
}

module.exports = {
    getAllOrder,
    getOrderById,
    getAllOrderOfUser,
    createOrderOfUser,
    updateOrderById,
    deleteOrderByID
}