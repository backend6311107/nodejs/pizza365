const mongoose = require("mongoose");
const userModel = require("../models/userModel");

const getAllUser = (req, res) => {
    const currentPage = parseInt(req.query.page) || 1; // trang hiện tại
    const perPage = parseInt(req.query.limit) || 2; // số lượng mục trên mỗi trang
    const skip = (currentPage - 1) * perPage; // chỉ số mục bắt đầu
    const limit = perPage; // số lượng mục lấy
    userModel.find()
        .skip(skip)
        .limit(limit)
        .exec((err, data) => {
            if (err) {
                return res.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return res.status(200).json({
                status: "Get all user successfully",
                data: data
            })
        })
}

const getAllUserLimit = async (req, res) => {
    try {
        const limitUser = req.query.limit;
        const dataUser = await userModel.find().limit(limitUser).exec();
        return res.status(200).json({
            status: "Get user limit successfully",
            data: dataUser
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }

};

const getAllUserSkip = async (req, res) => {
    try {
        const skipUser = req.query.skip;
        const dataUser = await userModel.find().skip(skipUser).exec();
        return res.status(200).json({
            status: "Get user skip successfully",
            data: dataUser
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }

};

const getAllUserSortFullName = async (req, res) => {
    try {
        const sortFullName = req.query.sort;
        const dataUser = await userModel.find().sort({ fullName: sortFullName }).exec();
        return res.status(200).json({
            status: "Get user sort by fullname successfully",
            data: dataUser
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

const getUserSkipLimit = async (req, res) => {
    try {
        const skipUser = req.query.skip;
        const limitUser = req.query.limit;
        const dataUser = await userModel.find().skip(skipUser).limit(limitUser).exec();
        return res.status(200).json({
            status: "Get user skip-limit successfully",
            data: dataUser
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

const getUserSortSkipLimit = async (req, res) => {
    try {
        const sortName = req.query.sort;
        const skipUser = req.query.skip;
        const limitUser = req.query.limit;

        const dataUser = await userModel.find().skip(skipUser).limit(limitUser).sort({ fullName: sortName }).exec();
        return res.status(200).json({
            status: "Get user sort-skip-limit successfully",
            data: dataUser
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

const getUserById = (req, res) => {
    const userID = req.params.userId;

    if (!mongoose.Types.ObjectId.isValid(userID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `UserID: ${courseID} không hợp lệ`
        })
    }

    userModel.findById(userID, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: `Get user by ID: ${userID} successfully`,
            data: data
        })
    })
};

const createUser = (req, res) => {
    const body = req.body;

    if (!body.fullName) {
        return res.status(400).json({
            status: "Bad request",
            message: "Full Name không hợp lệ"
        })
    }
    if (!body.email) {
        return res.status(400).json({
            status: "Bad request",
            message: "Email Không hợp lệ"
        })
    }
    if (!body.address) {
        return res.status(400).json({
            status: "Bad request",
            message: "Address Không hợp lệ"
        })
    }
    if (!body.phone) {
        return res.status(400).json({
            status: "Bad request",
            message: "Phone không hợp lệ"
        })
    }

    const newUser = {
        _id: mongoose.Types.ObjectId(),
        fullName: body.fullName,
        email: body.email,
        address: body.address,
        phone: body.phone
    }
    userModel.create(newUser, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(201).json({
            status: "Create new user successfully",
            data: data
        })
    })
};

const updateUserById = (req, res) => {
    const userID = req.params.userId;
    const body = req.body;

    if (!mongoose.Types.ObjectId.isValid(userID)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `User ID: ${userID} không hợp lệ`
        })
    }
    if (body.fullName !== undefined && body.fullName.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "fullName không hợp lệ"
        })
    }
    if (body.email !== undefined && body.email.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "Email Không hợp lệ"
        })
    }
    if (body.address !== undefined && body.address.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "Address không hợp lệ"
        })
    }
    if (body.phone !== undefined && body.phone.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "Phone không hợp lệ"
        })
    }

    const updateUser = {}
    if (body.fullName !== undefined) {
        updateUser.fullName = body.fullName;
    }
    if (body.email !== undefined) {
        updateUser.email = body.email;
    }
    if (body.address !== undefined) {
        updateUser.address = body.address;
    }
    if (body.phone !== undefined) {
        updateUser.phone = body.phone;
    }
    userModel.findByIdAndUpdate(userID, updateUser, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: `Update user ID: ${userID} successfully`,
            data: data
        })
    })
};

const deleteUserById = (req, res) => {
    const userID = req.params.userId;

    if (!mongoose.Types.ObjectId.isValid(userID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `UserID: ${userID} không hợp lệ`
        })
    }

    userModel.findByIdAndDelete(userID, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: `Delete user by ID: ${userID} successfully`
        })
    })
};

module.exports = {
    getAllUser,
    getAllUserLimit,
    getAllUserSkip,
    getAllUserSortFullName,
    getUserSkipLimit,
    getUserSortSkipLimit,
    getUserById,
    createUser,
    updateUserById,
    deleteUserById
}