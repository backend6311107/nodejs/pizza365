const mongoose = require("mongoose");
const drinkModel = require("../models/drinkModel");

const getAllDrink = (req, res) => {
    drinkModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: "Get all drink successfully.",
            data: data
        })
    })
}

const getDrinkById = (req, res) => {
    const drinkID = req.params.drinkId;

    if (!mongoose.Types.ObjectId.isValid(drinkID)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `drinkId: ${drinkID} Không hợp lệ !!!`
        })
    }

    drinkModel.findById(drinkID, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: `Internal server error`,
                message: err.message
            })
        }
        return res.status(200).json({
            status: `Get drinkId: ${drinkID} successfully.`,
            data: data
        })
    })
}

const createDrink = (req, res) => {
    const body = req.body;

    if (!body.maNuocUong) {
        return res.status(400).json({
            status: "Bad request !!!",
            message: "maNuocUong không hợp lệ !!!"
        })
    }
    if (!body.tenNuocUong) {
        return res.status(400).json({
            status: "Bad request !",
            message: "tenNuocUong không hợp lệ !!!"
        })
    }
    if (isNaN(body.donGia) || body.donGia <= 0) {
        return res.status(400).json({
            status: "Bad request !!!",
            message: "donGia không hợp lệ !!!"
        })
    }
    const newDrink = {
        _id: mongoose.Types.ObjectId(),
        maNuocUong: body.maNuocUong,
        tenNuocUong: body.tenNuocUong,
        donGia: body.donGia
    }
    drinkModel.create(newDrink, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                messsage: err.message
            })
        }
        return res.status(201).json({
            status: `Create new drink successfully.`,
            data: data
        })
    })
}

const updateDrinkById = (req, res) => {
    const drinkID = req.params.drinkId;
    const body = req.body;

    if (!mongoose.Types.ObjectId.isValid(drinkID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `DrinkId: ${drinkID} không hợp lệ.`
        })
    }
    if (body.maNuocUong !== undefined && body.maNuocUong.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "maNuocUong không hợp lệ !!!"
        })
    }
    if (body.tenNuocUong !== undefined && body.tenNuocUong.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "tenNuocUong không hợp lệ !!!"
        })
    }
    if (!body.donGia) {
        return res.status(400).json({
            status: "Bad request",
            message: `donGia không hợp lệ !!!`
        })
    }
    if (body.donGia !== undefined && (isNaN(body.donGia) || body.donGia <= 0)) {
        return res.status(400).json({
            status: "Bad request",
            message: "donGia không hợp lệ !!!"
        })
    }

    const updateDrink = {}
    if (body.maNuocUong !== undefined) {
        updateDrink.maNuocUong = body.maNuocUong;
    }
    if (body.tenNuocUong !== undefined) {
        updateDrink.tenNuocUong = body.tenNuocUong;
    }
    if (body.donGia !== undefined) {
        updateDrink.donGia = body.donGia;
    }

    console.log(updateDrink);
    drinkModel.findByIdAndUpdate(drinkID, updateDrink, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: `Update drinkId: ${drinkID} successfully.`,
            data: data
        })
    })
}

const deleteDrinkById = (req, res) => {
    const drinkID = req.params.drinkId;

    if (!mongoose.Types.ObjectId.isValid(drinkID)) {
        return res.status(400).json({
            status: "Bad request !!!",
            message: `DrinkID: ${drinkID} không hợp lệ !!!`
        })
    }

    drinkModel.findByIdAndDelete(drinkID, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error.",
                message: err.message
            })
        }
        return res.status(200).json({
            status: `Delete drinkId: ${drinkID} successfully.`
        })
    })
}

module.exports = {
    getAllDrink,
    getDrinkById,
    createDrink,
    updateDrinkById,
    deleteDrinkById
}