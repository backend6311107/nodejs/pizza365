const mongoose = require("mongoose");
const voucherModel = require("../models/voucherModel");

const getAllVoucher = (req, res) => {
    voucherModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: "Get all voucher successfully.",
            data: data
        })
    })
}

const getVoucherById = (req, res) => {
    const voucherID = req.params.voucherId;

    if (!mongoose.Types.ObjectId.isValid(voucherID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Voucher ID: ${voucherID} không hợp lệ !!!`
        })
    }

    voucherModel.findById(voucherID, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: `Get Voucher By ID: ${voucherID} successfully.`,
            data: data
        })
    })
}

const createVoucher = (req, res) => {
    const body = req.body;

    if (!body.maVoucher) {
        return res.status(400).json({
            status: "Bad request",
            message: `maVoucher không hợp lệ !!!`
        })
    }
    if (!body.phanTramGiamGia || (isNaN(body.phanTramGiamGia) || body.phanTramGiamGia < 0)) {
        return res.status(400).json({
            status: "Bad request",
            message: `phanTramGiamGia không hợp lệ !!!`
        })
    }

    const newVoucher = {
        _id: mongoose.Types.ObjectId(),
        maVoucher: body.maVoucher,
        phanTramGiamGia: body.phanTramGiamGia,
        ghiChu: body.ghiChu
    }
    voucherModel.create(newVoucher, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(201).json({
            status: "Create new voucher successfully.",
            data: data
        })
    })
}

const updateVoucherById = (req, res) => {
    const voucherID = req.params.voucherId;
    const body = req.body

    if (!mongoose.Types.ObjectId.isValid(voucherID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Voucher ID: ${voucherID} không hợp lệ !!!`
        })
    }
    if (body.maVoucher !== undefined && body.maVoucher.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: `Mã voucher không hợp lệ !!!`
        })
    }
    // if (!body.phanTramGiamGia) {
    //     return res.status(400).json({
    //         status: "Bad request",
    //         message: `Phan tram giam gia khong hop le`
    //     })
    // }
    if (body.phanTramGiamGia !== undefined && (isNaN(body.phanTramGiamGia) || body.phanTramGiamGia < 0)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Phần trăm giảm giá không hợp lệ !!!`
        })
    }

    const updateVoucher = {}
    if (body.maVoucher !== undefined) {
        updateVoucher.maVoucher = body.maVoucher;
    }
    if (body.phanTramGiamGia !== undefined) {
        updateVoucher.phanTramGiamGia = body.phanTramGiamGia;
    }
    if (body.ghiChu !== undefined) {
        updateVoucher.ghiChu = body.ghiChu;
    }
    voucherModel.findByIdAndUpdate(voucherID, updateVoucher, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: `Internal server error`,
                message: err.message
            })
        }
        return res.status(200).json({
            status: `Update voucher by ID: ${voucherID} successfully.`,
            data: data
        })
    })
}

const deleteVoucherById = (req, res) => {
    const voucherID = req.params.voucherId;

    if (!mongoose.Types.ObjectId.isValid(voucherID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Voucher ID: ${voucherID} không hợp lệ !!!`
        })
    }

    voucherModel.findByIdAndDelete(voucherID, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: `Delete voucher by ID: ${voucherID} successfully.`
        })
    })
}

module.exports = {
    getAllVoucher,
    getVoucherById,
    createVoucher,
    updateVoucherById,
    deleteVoucherById
}