const { timeStamp } = require("console");
const mongoose = require("mongoose");
const { stringify } = require("querystring");
const Schema = mongoose.Schema;

const drinkSchema = new Schema({
    maNuocUong: {
        type: String,
        required: true,
        unique: true
    },
    tenNuocUong: {
        type: String,
        required: true
    },
    donGia: {
        type: Number,
        required: true
    }
}, {
    timestamps: true
})
module.exports = mongoose.model("Drink", drinkSchema);