const mongoose = require("mongoose");
/**
 Sử dụng thư viện rand-token để tạo các chuỗi ngẫu nhiên có độ dài xác định
 Để gọi được thư viện được mở terminal cài: npm install rand-token  
**/
const randtoken = require("rand-token");
const Schema = mongoose.Schema;
const orderSchema = new Schema({
    orderCode: {
        type: String,
        default: function () {
            return randtoken.generate(5);
        },
        unique: true,
    },
    pizzaSize: {
        type: String,
        required: true
    },
    pizzaType: {
        type: String,
        required: true
    },
    voucher: {
        type: mongoose.Types.ObjectId,
        ref: "Voucher"
    },
    drink: {
        type: mongoose.Types.ObjectId,
        ref: "Drink"
    },
    status: {
        type: String,
        required: true
    }
}, {
    timestamps: true
})
module.exports = mongoose.model("Order", orderSchema);