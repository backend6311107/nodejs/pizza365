const getAllOrderMiddleware = (req, res, next) => {
    console.log("Get all order middleware");
    next();
}

const getOrderByIdMiddleware = (req, res, next) => {
    console.log("Get order by ID middleware");
    next();
}

const getAllOrderOfUserMiddleware = (req, res, next) => {
    console.log("Get all order of user middleware");
    next();
}

const createOrderOfUserMiddleware = (req, res, next) => {
    console.log("Create order of user middleware");
    next();
}

const updateOrderByIdMiddleware = (req, res, next) => {
    console.log("Update order by ID middleware");
    next();
}

const deleteOrderByIDMiddleware = (req, res, next) => {
    console.log("Delete order by ID middleware");
    next();
}

module.exports = {
    getAllOrderMiddleware,
    getOrderByIdMiddleware,
    getAllOrderOfUserMiddleware,
    createOrderOfUserMiddleware,
    updateOrderByIdMiddleware,
    deleteOrderByIDMiddleware
}