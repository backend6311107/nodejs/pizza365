const getAllDrinkMiddleware = (req, res, next) => {
    console.log(`Get all drink middleware`);
    next();
}

const getDrinkIdMiddleware = (req, res, next) => {
    console.log(`Get drink by ID middleware`);
    next();
}

const createDrinkMiddleware = (req, res, next) => {
    console.log(`Create new drink middleware.`);
    next();
}

const updateDrinkMiddleware = (req, res, next) => {
    console.log(`Update drink by ID middleware`);
    next();
}

const deleteDrinkMiddleware = (req, res, next) => {
    console.log(`Delete drink by ID middleware`);
    next();
}

module.exports = {
    getAllDrinkMiddleware,
    getDrinkIdMiddleware,
    createDrinkMiddleware,
    updateDrinkMiddleware,
    deleteDrinkMiddleware
}