const getAllUserMiddleware = (req, res, next) => {
    console.log("Get all user middleware");
    next();
}

const getAllUserLimitMiddleware = (req, res, next) => {
    console.log("Get all user limit middleware");
    next();
}

const getAllUserSkipMiddleware = (req, res, next) => {
    console.log("Get all user skip middleware");
    next();
}

const getAllUserSortFullName = (req, res, next) => {
    console.log("Get all user sort by fullName middleware");
    next();
}

const getUserSkipLimitMiddleware = (req, res, next) => {
    console.log("Get all user skip-imit middleware");
    next();
}

const getUserSortSkipLimitMiddleware = (req, res, next) => {
    console.log("Get all user sort-skip-imit middleware");
    next();
}

const getUserByIdMiddleware = (req, res, next) => {
    console.log("Get user by ID middleware");
    next();
}

const createUserMiddleware = (req, res, next) => {
    console.log("Create new user middleware");
    next();
}

const updateUserByIdMiddleware = (req, res, next) => {
    console.log("Update user by ID middleware");
    next();
}

const deleteUserByIdMiddleware = (req, res, next) => {
    console.log("Delete user by ID middleware");
    next();
}

module.exports = {
    getAllUserMiddleware,
    getAllUserLimitMiddleware,
    getAllUserSkipMiddleware,
    getAllUserSortFullName,
    getUserSkipLimitMiddleware,
    getUserSortSkipLimitMiddleware,
    getUserByIdMiddleware,
    createUserMiddleware,
    updateUserByIdMiddleware,
    deleteUserByIdMiddleware
}