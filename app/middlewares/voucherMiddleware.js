const getAllVoucherMiddleware = (req, res, next) => {
    console.log("Get all voucher middleware.");
    next();
}

const getVoucherByIdMiddleware = (req, res, next) => {
    console.log("Get voucher by ID");
    next();
}

const createVoucherMiddleware = (req, res, next) => {
    console.log("Create new voucher middleware.");
    next();
}

const updateVoucherByIdMiddleware = (req, res, next) => {
    console.log("Update voucher by ID middleware.");
    next();
}

const deleteVoucherByIdMiddleware = (req, res, next) => {
    console.log("Delete voucher by ID middleware.");
    next();
}

module.exports = {
    getAllVoucherMiddleware,
    getVoucherByIdMiddleware,
    createVoucherMiddleware,
    updateVoucherByIdMiddleware,
    deleteVoucherByIdMiddleware
}